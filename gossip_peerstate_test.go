/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package tbft

import (
	"crypto/rand"
	"crypto/sha256"
	"testing"
	"time"

	"chainmaker.org/chainmaker/common/v2/msgbus"
	"chainmaker.org/chainmaker/logger/v2"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	tbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/tbft"
	"github.com/stretchr/testify/require"
)

var (
	tLogger *logger.CMLogger
	chainid = "chainId1"
	nodeid  = "nodeId1"
)

func init() {
	tLogger = logger.GetLogger(chainid)
}

func newTbftImpl() *ConsensusTBFTImpl {
	validators := []string{"validator0", "validator1"}
	valSet := &validatorSet{
		logger:     tLogger,
		Validators: validators,
	}
	return &ConsensusTBFTImpl{
		Id:             chainid,
		msgbus:         msgbus.NewMessageBus(),
		validatorSet:   valSet,
		ConsensusState: NewConsensusState(tLogger, nodeid),
	}
}
func TestGossipUpdateValidators(t *testing.T) {
	gs := newGossipService(tLogger, newTbftImpl())
	ps, ok := gs.peerStates["validator0"]
	require.NotNil(t, ps)
	require.True(t, ok)
	ps, ok = gs.peerStates["validator2"]
	require.Nil(t, ps)
	require.False(t, ok)

	validators := []string{"validator2", "validator3"}
	err := gs.addValidators(validators)
	require.Nil(t, err)
	ps, ok = gs.peerStates["validator2"]
	require.NotNil(t, ps)
	require.True(t, ok)
	ps, ok = gs.peerStates["validator3"]
	require.NotNil(t, ps)
	require.True(t, ok)

	err = gs.removeValidators(validators)
	require.Nil(t, err)
	ps, ok = gs.peerStates["validator0"]
	require.NotNil(t, ps)
	require.True(t, ok)
	ps, ok = gs.peerStates["validator1"]
	require.NotNil(t, ps)
	require.True(t, ok)
	ps, ok = gs.peerStates["validator2"]
	require.Nil(t, ps)
	require.False(t, ok)
	ps, ok = gs.peerStates["validator3"]
	require.Nil(t, ps)
	require.False(t, ok)
}

func TestPeerSendState(t *testing.T) {
	pss := NewPeerSendState(tLogger)
	require.Equal(t, pss.fibonacci(int64(0)), int64(1))
	require.Equal(t, pss.fibonacci(int64(1)), int64(1))
	require.Equal(t, pss.fibonacci(int64(2)), int64(2))
	require.Equal(t, pss.fibonacci(int64(3)), int64(3))
	require.Equal(t, pss.fibonacci(int64(4)), int64(5))
	require.Equal(t, pss.fibonacci(int64(5)), int64(8))
	require.Equal(t, pss.fibonacci(int64(6)), int64(13))
	require.Equal(t, pss.fibonacci(int64(7)), int64(21))
	require.Equal(t, pss.fibonacci(int64(8)), int64(34))
	require.Equal(t, pss.fibonacci(int64(9)), int64(55))
}

func TestSelectPeersSuccess(t *testing.T) {
	gs := newGossipService(tLogger, newTbftImpl())

	// add two node
	validators := []string{"validator2", "validator3"}
	_ = gs.addValidators(validators)
	gs.peerStates["validator0"].Round = 9
	gs.peerStates["validator1"].Round = 10
	gs.peerStates["validator2"].Round = 11
	gs.peerStates["validator3"].Round = 12

	gs.tbftImpl.Round = 11
	peer := gs.selectPeers()
	require.Equal(t, peer, "validator3")
}

func TestSelectPeersRandom(t *testing.T) {
	gs := newGossipService(tLogger, newTbftImpl())
	gs.peerStates["validator0"].Round = 10
	gs.peerStates["validator1"].Round = 8

	gs.tbftImpl.Round = 8
	peer := gs.selectPeers()
	require.Equal(t, peer, "validator0")

	// add two node
	validators := []string{"validator2", "validator3"}
	_ = gs.addValidators(validators)
	gs.peerStates["validator2"].Round = 11
	gs.peerStates["validator3"].Round = 9

	// node0.Round = 10
	// node1.Round = 8
	// node2.Round = 11
	// node3.Round = 9
	// only node0 and node2 are selected
	for i := 0; i < 10; i++ {
		peer = gs.selectPeers()
		require.NotEqual(t, peer, "validator1")
		//require.NotEqual(t, peer, "validator3")
	}
}

func TestSelectPeersNull(t *testing.T) {
	gs := newGossipService(tLogger, newTbftImpl())
	gs.peerStates["validator0"].Round = 10
	gs.peerStates["validator1"].Round = 8

	gs.tbftImpl.Round = 8
	peer := gs.selectPeers()
	require.Equal(t, peer, "validator0")

	// add two node
	validators := []string{"validator2", "validator3"}
	_ = gs.addValidators(validators)
	gs.peerStates["validator2"].Round = 11
	gs.peerStates["validator3"].Round = 9

	gs.tbftImpl.Round = 9
	peer = gs.selectPeers()
	require.Equal(t, peer, "validator2")

	// remove all peers
	validators = []string{"validator0", "validator1", "validator2", "validator3"}
	_ = gs.removeValidators(validators)
	peer = gs.selectPeers()
	require.Equal(t, peer, "")

}

func TestSendRoundQC(t *testing.T) {
	nodes := []string{
		"QmQZn3pZCcuEf34FSvucqkvVJEvfzpNjQTk17HS6CYMR35",
		"QmeRZz3AjhzydkzpiuuSAtmqt8mU8XcRH2hynQN4tLgYg6",
		"QmTSMcqwp4X6oPP5WrNpsMpotQMSGcxVshkGLJUhCrqGbu",
		"QmUryDgjNoxfMXHdDRFZ5Pe55R1vxTPA3ZgCteHze2ET27",
	}

	validatorSet := newValidatorSet(tLogger, nodes, 1)
	gs := newGossipService(tLogger, newTbftImpl())
	err := gs.addValidators(nodes)
	require.Nil(t, err)
	gs.tbftImpl.heightRoundVoteSet = newHeightRoundVoteSet(
		tLogger, 1, 0, validatorSet)

	blockHash := nilHash

	var height uint64 = 1
	for round := 0; round < 10; round++ {
		for _, id := range nodes {
			vote := NewVote(tbftpb.VoteType_VOTE_PRECOMMIT, id, height, int32(round), blockHash[:])
			_, _ = gs.tbftImpl.heightRoundVoteSet.addVote(vote)
		}
		gs.tbftImpl.Height = height
		gs.tbftImpl.Round = int32(round)
	}

	fetchQCProto := &tbftpb.FetchRoundQC{
		Id:     nodes[0],
		Height: 1,
		Round:  3,
	}
	gs.peerStates[nodes[0]].sendRoundQC(fetchQCProto)
}

func TestEnterPrecommitFromReplayWalSuccess(t *testing.T) {
	nodes := []string{
		"QmQZn3pZCcuEf34FSvucqkvVJEvfzpNjQTk17HS6CYMR35",
		"QmeRZz3AjhzydkzpiuuSAtmqt8mU8XcRH2hynQN4tLgYg6",
		"QmTSMcqwp4X6oPP5WrNpsMpotQMSGcxVshkGLJUhCrqGbu",
		"QmUryDgjNoxfMXHdDRFZ5Pe55R1vxTPA3ZgCteHze2ET27",
	}
	validatorSet := newValidatorSet(tLogger, nodes, 1)

	tbftImpl := newTbftImpl()
	tbftImpl.logger = tLogger
	tbftImpl.heightRoundVoteSet = newHeightRoundVoteSet(
		tLogger, 1, 0, validatorSet)

	var height uint64 = 1
	var round int32 = 1
	blockHash := sha256.Sum256(nil)
	_, _ = rand.Read(blockHash[:])

	block := &common.Block{
		Header: &common.BlockHeader{
			BlockHash: []byte(blockHash[:]),
		},
	}
	vote := NewVote(tbftpb.VoteType_VOTE_PRECOMMIT, nodes[0], height, round, blockHash[:])
	_, _ = tbftImpl.heightRoundVoteSet.addVote(vote)

	proposal := NewProposal(nodes[0], height, round, -1, block)

	tbftImpl.logger.Infof("enter precommit from replay wal")

	proposalVoteRemix := &tbftpb.ProposalVoteRemix{
		Proposal:      proposal,
		PrecommitVote: vote,
	}

	if err := tbftImpl.enterPrecommitFromReplayWal(proposalVoteRemix); err != nil {
		t.Errorf("enterPrecommitFromReplayWal() error = %v, but expecte null", err)
	}
}

func TestEnterPrecommitFromReplayWalFailed(t *testing.T) {
	nodes := []string{
		"QmQZn3pZCcuEf34FSvucqkvVJEvfzpNjQTk17HS6CYMR35",
		"QmeRZz3AjhzydkzpiuuSAtmqt8mU8XcRH2hynQN4tLgYg6",
		"QmTSMcqwp4X6oPP5WrNpsMpotQMSGcxVshkGLJUhCrqGbu",
		"QmUryDgjNoxfMXHdDRFZ5Pe55R1vxTPA3ZgCteHze2ET27",
	}
	validatorSet := newValidatorSet(tLogger, nodes, 1)

	tbftImpl := newTbftImpl()
	tbftImpl.logger = tLogger
	tbftImpl.heightRoundVoteSet = newHeightRoundVoteSet(
		tLogger, 1, 0, validatorSet)

	id := "QmQZn3pZCcuEf34FSvucqkvVJEvfzpNjQTk17HS6CYMR35"
	var height uint64 = 1
	var round int32 = 1
	blockHash := sha256.Sum256([]byte("blockHash"))
	_, _ = rand.Read(blockHash[:])

	voteHash := sha256.Sum256([]byte("voteHash"))
	_, _ = rand.Read(voteHash[:])

	block := &common.Block{
		Header: &common.BlockHeader{
			BlockHash: []byte(blockHash[:]),
		},
	}
	vote := NewVote(tbftpb.VoteType_VOTE_PRECOMMIT, id, height, round, voteHash[:])
	_, _ = tbftImpl.heightRoundVoteSet.addVote(vote)

	proposal := NewProposal(id, height, round, -1, block)

	tbftImpl.logger.Infof("enter precommit from replay wal")

	proposalVoteRemix := &tbftpb.ProposalVoteRemix{
		Proposal:      proposal,
		PrecommitVote: vote,
	}

	if err := tbftImpl.enterPrecommitFromReplayWal(proposalVoteRemix); err == nil {
		t.Errorf("enterPrecommitFromReplayWal() error = %v, but expecte error", err)
	}
}

func TestRecvState(t *testing.T) {
	gs := newGossipService(tLogger, newTbftImpl())
	nodeId := "validator0"
	// original peer state
	gs.peerStates[nodeId] = &PeerStateService{Id: nodeId, Height: 5, Round: 1, Step: tbftpb.Step_PRECOMMIT,
		stateC: make(chan *tbftpb.GossipState, 10),
	}
	recStateCh := gs.peerStates[nodeId].stateC

	// add higher height state
	state := &tbftpb.GossipState{Id: nodeId, Height: 6, Round: 0, Step: tbftpb.Step_PROPOSE}
	go gs.procRecvState(state)
	receiveState := <-recStateCh
	require.Equal(t, uint64(6), receiveState.Height)
	require.Equal(t, int32(0), receiveState.Round)
	require.Equal(t, tbftpb.Step_PROPOSE, receiveState.Step)

	// add higher round state
	state = &tbftpb.GossipState{Id: nodeId, Height: 5, Round: 2, Step: tbftpb.Step_PROPOSE}
	go gs.procRecvState(state)
	receiveState = <-recStateCh
	require.Equal(t, uint64(5), receiveState.Height)
	require.Equal(t, int32(2), receiveState.Round)
	require.Equal(t, tbftpb.Step_PROPOSE, receiveState.Step)

	// add higher step state
	state = &tbftpb.GossipState{Id: nodeId, Height: 5, Round: 1, Step: tbftpb.Step_COMMIT}
	go gs.procRecvState(state)
	receiveState = <-recStateCh
	require.Equal(t, uint64(5), receiveState.Height)
	require.Equal(t, int32(1), receiveState.Round)
	require.Equal(t, tbftpb.Step_COMMIT, receiveState.Step)

	// add equal step state
	state = &tbftpb.GossipState{Id: nodeId, Height: 5, Round: 1, Step: tbftpb.Step_PRECOMMIT}
	go gs.procRecvState(state)
	receiveState = <-recStateCh
	require.Equal(t, uint64(5), receiveState.Height)
	require.Equal(t, int32(1), receiveState.Round)
	require.Equal(t, tbftpb.Step_PRECOMMIT, receiveState.Step)

	// add lower height state
	state = &tbftpb.GossipState{Id: nodeId, Height: 4, Round: 1, Step: tbftpb.Step_COMMIT}
	go gs.procRecvState(state)
	time.Sleep(time.Second)
	select {
	case receiveState = <-recStateCh:
		require.Fail(t, "should not happen")
	default:
	}
	// add lower round state
	state = &tbftpb.GossipState{Id: nodeId, Height: 5, Round: 0, Step: tbftpb.Step_COMMIT}
	go gs.procRecvState(state)
	time.Sleep(time.Second)
	select {
	case receiveState = <-recStateCh:
		require.Fail(t, "should not happen")
	default:
	}
	// add lower step state
	state = &tbftpb.GossipState{Id: nodeId, Height: 5, Round: 1, Step: tbftpb.Step_PROPOSE}
	go gs.procRecvState(state)
	time.Sleep(time.Second)
	select {
	case receiveState = <-recStateCh:
		require.Fail(t, "should not happen")
	default:
	}
}
